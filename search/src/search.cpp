#include <iostream>
using namespace std;

/**
 * table can be sorted or unsorted
 */
int LinearSearch(int table[], int length, int element) {
	int var = 0;
	while (var < length && table[var] != element) {
		++var;
	}
	if (var == length) {
		return -1;
	}
	return var + 1;
}

/**
 * table is sorted in ascending order
 */
int BinarySearch(int table[], int length, int element) {
	int left = 0;
	int right = length - 1;
	int middle = left + ((right - left) / 2);

	while (left <= right) {
		if (element == table[middle])
			return middle;
		else if (element < table[middle])
			right = (middle - 1);
		else
			left = middle + 1;
		middle = left + ((right - left) / 2);
	}
	return -1;
}

/**
 * table is sorted in ascending order
 */
int BinarySearchRec(int table[], int left, int right, int element) {
	if (left <= right) {
		int middle = left + ((right - left) / 2);

		if (element == table[middle])
			return middle;
		else if (element < table[middle])
			return BinarySearchRec(table, left, middle - 1, element);
		else
			return BinarySearchRec(table, middle + 1, right, element);
	}
	return -1;
}

int main() {
	int table[] = { 5, 7, 13, 24, 36, 50, 107 };
	//int result = BinarySearch(table, 7, 36);
	int result = BinarySearchRec(table, 0, 6, 36);
	cout << result << endl;

	return 0;
}
