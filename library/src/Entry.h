/*
 * Entry.h
 *
 *  Created on: Apr 20, 2015
 *      Author: Beata
 */

#ifndef ENTRY_H_
#define ENTRY_H_

#include <iostream>
using namespace std;

template<class K, class T>
class Entry {
private:
	K* searchKey;
	T* element;
public:
	Entry();
	Entry(K* newSearchKey, T* newElement);
	T* getElement();
	void setElement(T* element);
	K* getSearchKey();

	int compare(const Entry& person) const;
	bool operator ==(const Entry& d) const;
	bool operator <(const Entry& d) const;
};

template<class K, class T>
Entry<K, T>::Entry() {
	searchKey = NULL;
	element = NULL;
}

template<class K, class T>
Entry<K, T>::Entry(K* newSearchKey, T* newElement) {
	searchKey = newSearchKey;
	element = newElement;
}

template<class K, class T>
T* Entry<K, T>::getElement() {
	return element;
}

template<class K, class T>
void Entry<K, T>::setElement(T* element) {
	this->element = element;
}

template<class K, class T>
K* Entry<K, T>::getSearchKey() {
	return searchKey;
}

template<class K, class T>
int Entry<K, T>::compare(const Entry& entry) const {
	if (*searchKey < *(entry.searchKey)) {
		return -1;
	} else if (*searchKey > *(entry.searchKey)) {
		return 1;
	}
	return 0;
}

template<class K, class T>
bool Entry<K, T>::operator ==(const Entry& d) const {
	return !compare(d);
}

template<class K, class T>
bool Entry<K, T>::operator <(const Entry& d) const {
	return compare(d) < 0;
}

#endif /* ENTRY_H_ */
