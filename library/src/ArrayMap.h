/*
 * ArrayMap.h
 *
 *  Created on: Feb 11, 2016
 *      Author: bkubiak
 */

#ifndef ARRAYMAP_H_
#define ARRAYMAP_H_

#include <iostream>
using namespace std;

#include "Entry.h"

template<class K, class T>
class ArrayMap {
private:
	Entry<K, T>** entries;
	int length;
	int maxLength;
public:
	ArrayMap();
	~ArrayMap();

	unsigned int size();

	bool add(K* searchKey, T* element);

	T* get(K* searchKey);

	T* remove(K* searchKey);
};

template<class K, class T>
ArrayMap<K, T>::ArrayMap() {
	//TODO
}

template<class K, class T>
ArrayMap<K, T>::~ArrayMap() {
	//TODO
}

template<class K, class T>
unsigned int ArrayMap<K, T>::size() {
	//TODO
	return 0;
}

template<class K, class T>
bool ArrayMap<K, T>::add(K* searchKey, T* element) {
	//TODO
	return false;
}

template<class K, class T>
T* ArrayMap<K, T>::get(K* searchKey) {
	//TODO
	return NULL;
}

template<class K, class T>
T* ArrayMap<K, T>::remove(K* searchKey) {
	//TODO
	return NULL;
}

#endif /* ARRAYMAP_H_ */
