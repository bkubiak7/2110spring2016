#include <iostream>
using namespace std;
#include "gtest/gtest.h"
/*#include "../src/Book.h"
#include "../src/Books.h"

TEST(Books_Books, constructBookTitleAuthor) {
	Book book("DS&A", "Carrano");
	EXPECT_EQ("DS&A", book.getTitle());
	EXPECT_EQ("Carrano", book.getAuthor());
}

TEST(Books_add, addOneBook) {
	Books books; //creates object of type Books
	Book book("DS&A", "Carrano"); //creates a book
	bool added = books.add(book); //adds the book to the collection of books
	//addition should be successful
	EXPECT_TRUE(added);	//EXPECT_TRUE comes from Google Test framework
	EXPECT_EQ(1, books.getAmount());//EXPECT_EQ comes from Google Test framework
	//EXPECT_EQ checks if the collection of books contains one book
}

TEST(Books_add, addThreeBooks) {
	Books books;
	Book book1("DS&A", "Carrano");
	Book book2("c++", "Carrano");
	Book book3("Java", "Carrano");
	bool added = books.add(book1);
	EXPECT_TRUE(added);
	added = books.add(book2);
	EXPECT_TRUE(added);
	added = books.add(book3);
	EXPECT_TRUE(added);
	EXPECT_EQ(3, books.getAmount());
}

TEST(Books_add, addSixBooks) {
	Books books;
	Book book1("DS&A", "Carrano");
	Book book2("c++", "Carrano");
	Book book3("Java", "Carrano");
	Book book4("Python", "Carrano");
	Book book5("Ruby", "Carrano");
	Book book6("Perl", "Carrano");
	bool added = books.add(book1);
	EXPECT_TRUE(added);
	added = books.add(book2);
	EXPECT_TRUE(added);
	added = books.add(book3);
	EXPECT_TRUE(added);
	added = books.add(book4);
	EXPECT_TRUE(added);
	added = books.add(book5);
	EXPECT_TRUE(added);
	added = books.add(book6);
	EXPECT_FALSE(added);
	EXPECT_EQ(5, books.getAmount());
}

TEST(Books_add, addBookThatWasAlreadyAdded) {
	Books books;
	Book book1("DS&A", "Carrano");
	Book book2("DS&A", "Carrano");
	bool added = books.add(book1);
	EXPECT_TRUE(added);
	added = books.add(book2);
	EXPECT_FALSE(added);
	EXPECT_EQ(1, books.getAmount());
}

TEST(Books_add, addFiveBooksRemoveOneAndAddNewOne) {
	Books books;
	Book book1("DS&A", "Carrano");
	Book book2("c++", "Carrano");
	Book book3("Java", "Carrano");
	Book book4("Python", "Carrano");
	Book book5("Ruby", "Carrano");
	Book book6("Perl", "Carrano");
	bool added = books.add(book1);
	EXPECT_TRUE(added);
	added = books.add(book2);
	EXPECT_TRUE(added);
	added = books.add(book3);
	EXPECT_TRUE(added);
	added = books.add(book4);
	EXPECT_TRUE(added);
	added = books.add(book5);
	EXPECT_TRUE(added);

	books.remove("c++");

	added = books.add(book6);
	EXPECT_TRUE(added);
	EXPECT_EQ(5, books.getAmount());
}

TEST(Books_remove, addOneBookAndRemoveIt) {
	Books books;
	Book book("DS&A", "Carrano");
	books.add(book);
	Book* removedBook = books.remove("DS&A");
	EXPECT_EQ(0, books.getAmount());
	EXPECT_EQ("DS&A", (*removedBook).getTitle());
}

TEST(Books_remove, addOneBookAndRemoveDifferentOne) {
	Books books;
	Book book("DS&A", "Carrano");
	books.add(book);
	Book* removedBook = books.remove("c++");
	EXPECT_EQ(1, books.getAmount());
	EXPECT_EQ(NULL, removedBook);
}

TEST(Books_remove, addThreeBooksAndRemoveTheFirstAdded) {
	Books books;
	Book book1("DS&A", "Carrano");
	Book book2("c++", "Carrano");
	Book book3("Java", "Carrano");
	books.add(book1);
	books.add(book2);
	books.add(book3);
	Book* removedBook = books.remove("DS&A");

	EXPECT_EQ("DS&A", (*removedBook).getTitle());
	EXPECT_EQ(2, books.getAmount());
}

TEST(Books_remove, addThreeBooksAndRemoveTheSecondAdded) {
	Books books;
	Book book1("DS&A", "Carrano");
	Book book2("c++", "Carrano");
	Book book3("Java", "Carrano");
	books.add(book1);
	books.add(book2);
	books.add(book3);
	Book* removedBook = books.remove("c++");

	EXPECT_EQ("c++", (*removedBook).getTitle());
	EXPECT_EQ(2, books.getAmount());
}

TEST(Books_remove, addThreeBooksAndRemoveTheThirdAdded) {
	Books books;
	Book book1("DS&A", "Carrano");
	Book book2("c++", "Carrano");
	Book book3("Java", "Carrano");
	books.add(book1);
	books.add(book2);
	books.add(book3);
	Book* removedBook = books.remove("Java");

	EXPECT_EQ("Java", (*removedBook).getTitle());
	EXPECT_EQ(2, books.getAmount());
}

TEST(Books_remove, removeFromEmptyCollection) {
	Books books;
	Book* removedBook = books.remove("DS&A");
	EXPECT_EQ(NULL, removedBook);
}

TEST(Books_get, addOneBookAndGetIt) {
	Books books;
	Book book("DS&A", "Carrano");
	books.add(book);
	Book* retrievedBook = books.get("DS&A");
	EXPECT_EQ("DS&A", (*retrievedBook).getTitle());
}

TEST(Books_get, addOneBookAndGetDifferentOne) {
	Books books;
	Book book("DS&A", "Carrano");
	books.add(book);
	Book* retrievedBook = books.get("c++");
	EXPECT_EQ(NULL, retrievedBook);
}

TEST(Books_get, addThreeBooksAndGetTheFirstAdded) {
	Books books;
	Book book1("DS&A", "Carrano");
	Book book2("c++", "Carrano");
	Book book3("Java", "Carrano");
	books.add(book1);
	books.add(book2);
	books.add(book3);
	Book* retrievedBook = books.get("DS&A");

	EXPECT_EQ("DS&A", (*retrievedBook).getTitle());
}

TEST(Books_get, addThreeBooksAndGetTheSecondAdded) {
	Books books;
	Book book1("DS&A", "Carrano");
	Book book2("c++", "Carrano");
	Book book3("Java", "Carrano");
	books.add(book1);
	books.add(book2);
	books.add(book3);
	Book* retrievedBook = books.get("c++");

	EXPECT_EQ("c++", (*retrievedBook).getTitle());
}

TEST(Books_get, addThreeBooksAndGetTheThirdAdded) {
	Books books;
	Book book1("DS&A", "Carrano");
	Book book2("c++", "Carrano");
	Book book3("Java", "Carrano");
	books.add(book1);
	books.add(book2);
	books.add(book3);
	Book* retrievedBook = books.get("Java");

	EXPECT_EQ("Java", (*retrievedBook).getTitle());
}

TEST(Books_get, getFromEmptyCollection) {
	Books books;
	Book* retrievedBook = books.get("DS&A");
	EXPECT_EQ(NULL, retrievedBook);
}*/
