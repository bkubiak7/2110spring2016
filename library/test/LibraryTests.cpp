/*
 * LibraryTests.cpp
 *
 *  Created on: Feb 4, 2016
 *      Author: bkubiak
 */
#include <iostream>
using namespace std;
#include "gtest/gtest.h"

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

