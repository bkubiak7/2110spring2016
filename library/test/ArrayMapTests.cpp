#include <iostream>
using namespace std;
#include "gtest/gtest.h"
/*#include "../src/Book.h"
#include "../src/ArrayMap.h"

TEST(ArrayMap_Books, constructBookTitleAuthor) {
	Book book("DS&A", "Carrano");
	EXPECT_EQ("DS&A", book.getTitle());
	EXPECT_EQ("Carrano", book.getAuthor());
}

TEST(ArrayMap_add, addOneBook) {
	ArrayMap<string, Book> map; //creates object of type Books
	Book book("DS&A", "Carrano"); //creates a book
	string title = "DS&A";
	bool added = map.add(&title, &book); //adds the book to the collection of books
	//addition should be successful
	EXPECT_TRUE(added);	//EXPECT_TRUE comes from Google Test framework
	EXPECT_EQ(1, map.size());	//EXPECT_EQ comes from Google Test framework
	//EXPECT_EQ checks if the collection of books contains one book
}

TEST(ArrayMap_add, addThreeBooks) {
	ArrayMap<string, Book> map;
	Book book1("DS&A", "Carrano");
	Book book2("c++", "Carrano");
	Book book3("Java", "Carrano");
	string title1 = "DS&A";
	bool added = map.add(&title1, &book1);
	EXPECT_TRUE(added);
	string title2 = "c++";
	added = map.add(&title2, &book2);
	EXPECT_TRUE(added);
	string title3 = "Java";
	added = map.add(&title3, &book3);
	EXPECT_TRUE(added);
	EXPECT_EQ(3, map.size());
}

TEST(ArrayMap_add, addSixBooks) {
	ArrayMap<string, Book> map;
	Book book1("DS&A", "Carrano");
	Book book2("c++", "Carrano");
	Book book3("Java", "Carrano");
	Book book4("Python", "Carrano");
	Book book5("Ruby", "Carrano");
	Book book6("Perl", "Carrano");
	string title1 = "DS&A";
	string title2 = "c++";
	string title3 = "Java";
	string title4 = "Python";
	string title5 = "Ruby";
	string title6 = "Perl";
	bool added = map.add(&title1, &book1);
	EXPECT_TRUE(added);
	added = map.add(&title2, &book2);
	EXPECT_TRUE(added);
	added = map.add(&title3, &book3);
	EXPECT_TRUE(added);
	added = map.add(&title4, &book4);
	EXPECT_TRUE(added);
	added = map.add(&title5, &book5);
	EXPECT_TRUE(added);
	added = map.add(&title6, &book6);
	EXPECT_FALSE(added);
	EXPECT_EQ(5, map.size());
}

TEST(ArrayMap_add, addBookThatWasAlreadyAdded) {
	ArrayMap<string, Book> map;
	Book book1("DS&A", "Carrano");
	Book book2("DS&A", "Carrano");
	string title1 = "DS&A";
	string title2 = "DS&A";
	bool added = map.add(&title1, &book1);
	EXPECT_TRUE(added);
	added = map.add(&title2, &book2);
	EXPECT_FALSE(added);
	EXPECT_EQ(1, map.size());
}

TEST(ArrayMap_add, addFiveBooksRemoveOneAndAddNewOne) {
	ArrayMap<string, Book> map;
	Book book1("DS&A", "Carrano");
	Book book2("c++", "Carrano");
	Book book3("Java", "Carrano");
	Book book4("Python", "Carrano");
	Book book5("Ruby", "Carrano");
	Book book6("Perl", "Carrano");
	string title1 = "DS&A";
	string title2 = "c++";
	string title3 = "Java";
	string title4 = "Python";
	string title5 = "Ruby";
	string title6 = "Perl";
	bool added = map.add(&title1, &book1);
	EXPECT_TRUE(added);
	added = map.add(&title2, &book2);
	EXPECT_TRUE(added);
	added = map.add(&title3, &book3);
	EXPECT_TRUE(added);
	added = map.add(&title4, &book4);
	EXPECT_TRUE(added);
	added = map.add(&title5, &book5);
	EXPECT_TRUE(added);
	string title = "c++";
	map.remove(&title);

	added = map.add(&title6, &book6);
	EXPECT_TRUE(added);
	EXPECT_EQ(5, map.size());
}

TEST(ArrayMap_remove, addOneBookAndRemoveIt) {
	ArrayMap<string, Book> map;
	Book book1("DS&A", "Carrano");
	string title1 = "DS&A";
	map.add(&title1, &book1);
	string title = "DS&A";
	Book* removedBook = map.remove(&title);
	EXPECT_EQ(0, map.size());
	EXPECT_EQ("DS&A", (*removedBook).getTitle());
}

TEST(ArrayMap_remove, addOneBookAndRemoveDifferentOne) {
	ArrayMap<string, Book> map;
	Book book1("DS&A", "Carrano");
	string title1 = "DS&A";
	map.add(&title1, &book1);
	string title = "c++";

	Book* removedBook = map.remove(&title);
	EXPECT_EQ(1, map.size());
	EXPECT_EQ(NULL, removedBook);
}

TEST(ArrayMap_remove, addThreeBooksAndRemoveTheFirstAdded) {
	ArrayMap<string, Book> map;
	Book book1("DS&A", "Carrano");
	Book book2("c++", "Carrano");
	Book book3("Java", "Carrano");
	string title1 = "DS&A";
	string title2 = "c++";
	string title3 = "Java";
	map.add(&title1, &book1);
	map.add(&title2, &book2);
	map.add(&title3, &book3);
	string title = "DS&A";
	Book* removedBook = map.remove(&title);

	EXPECT_EQ("DS&A", (*removedBook).getTitle());
	EXPECT_EQ(2, map.size());
}

TEST(ArrayMap_remove, addThreeBooksAndRemoveTheSecondAdded) {
	ArrayMap<string, Book> map;
	Book book1("DS&A", "Carrano");
	Book book2("c++", "Carrano");
	Book book3("Java", "Carrano");
	string title1 = "DS&A";
	string title2 = "c++";
	string title3 = "Java";
	map.add(&title1, &book1);
	map.add(&title2, &book2);
	map.add(&title3, &book3);
	string title = "c++";
	Book* removedBook = map.remove(&title);

	EXPECT_EQ("c++", (*removedBook).getTitle());
	EXPECT_EQ(2, map.size());
}

TEST(ArrayMap_remove, addThreeBooksAndRemoveTheThirdAdded) {
	ArrayMap<string, Book> map;
	Book book1("DS&A", "Carrano");
	Book book2("c++", "Carrano");
	Book book3("Java", "Carrano");
	string title1 = "DS&A";
	string title2 = "c++";
	string title3 = "Java";
	map.add(&title1, &book1);
	map.add(&title2, &book2);
	map.add(&title3, &book3);
	string title = "Java";
	Book* removedBook = map.remove(&title);

	EXPECT_EQ("Java", (*removedBook).getTitle());
	EXPECT_EQ(2, map.size());
}

TEST(ArrayMap_remove, removeFromEmptyCollection) {
	ArrayMap<string, Book> map;
	string title = "DS&A";
	Book* removedBook = map.remove(&title);
	EXPECT_EQ(NULL, removedBook);
}

TEST(ArrayMap_get, addOneBookAndGetIt) {
	ArrayMap<string, Book> map;
	Book book1("DS&A", "Carrano");
	string title1 = "DS&A";
	map.add(&title1, &book1);
	string title = "DS&A";
	Book* retrievedBook = map.get(&title);
	EXPECT_EQ("DS&A", (*retrievedBook).getTitle());
}

TEST(ArrayMap_get, addOneBookAndGetDifferentOne) {
	ArrayMap<string, Book> map;
	Book book("DS&A", "Carrano");
	Book book1("DS&A", "Carrano");
	string title1 = "DS&A";
	map.add(&title1, &book1);
	string title = "c++";
	Book* retrievedBook = map.get(&title);
	EXPECT_EQ(NULL, retrievedBook);
}

TEST(ArrayMap_get, addThreeBooksAndGetTheFirstAdded) {
	ArrayMap<string, Book> map;
	Book book1("DS&A", "Carrano");
	Book book2("c++", "Carrano");
	Book book3("Java", "Carrano");
	string title1 = "DS&A";
	string title2 = "c++";
	string title3 = "Java";
	map.add(&title1, &book1);
	map.add(&title2, &book2);
	map.add(&title3, &book3);
	string title = "DS&A";
	Book* retrievedBook = map.get(&title);

	EXPECT_EQ("DS&A", (*retrievedBook).getTitle());
}

TEST(ArrayMap_get, addThreeBooksAndGetTheSecondAdded) {
	ArrayMap<string, Book> map;
	Book book1("DS&A", "Carrano");
	Book book2("c++", "Carrano");
	Book book3("Java", "Carrano");
	string title1 = "DS&A";
	string title2 = "c++";
	string title3 = "Java";
	map.add(&title1, &book1);
	map.add(&title2, &book2);
	map.add(&title3, &book3);
	string title = "c++";
	Book* retrievedBook = map.get(&title);

	EXPECT_EQ("c++", (*retrievedBook).getTitle());
}

TEST(ArrayMap_get, addThreeBooksAndGetTheThirdAdded) {
	ArrayMap<string, Book> map;
	Book book1("DS&A", "Carrano");
	Book book2("c++", "Carrano");
	Book book3("Java", "Carrano");
	string title1 = "DS&A";
	string title2 = "c++";
	string title3 = "Java";
	map.add(&title1, &book1);
	map.add(&title2, &book2);
	map.add(&title3, &book3);
	string title = "Java";
	Book* retrievedBook = map.get(&title);

	EXPECT_EQ("Java", (*retrievedBook).getTitle());
}

TEST(ArrayMap_get, getFromEmptyCollection) {
	ArrayMap<string, Book> map;
	string title = "DS&A";
	Book* retrievedBook = map.get(&title);
	EXPECT_EQ(NULL, retrievedBook);
}*/
