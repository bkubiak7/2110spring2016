/*
 * Widget.h
 *
 *  Created on: Feb 28, 2015
 *      Author: Beata
 */

#ifndef WIDGET_H_
#define WIDGET_H_

class Widget {
private:
	float cost;
	unsigned int amount;
public:
	unsigned int getAmount();
	void setAmount(unsigned int amount);
	float getCost();
	void setCost(float cost);
};

#endif /* WIDGET_H_ */
