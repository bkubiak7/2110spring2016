/*
 * FIFOStrategy.h
 *
 *  Created on: Mar 1, 2015
 *      Author: B
 */

#ifndef FIFOSTRATEGY_H_
#define FIFOSTRATEGY_H_

#include "Strategy.h"
#include "Widget.h"
#include "X:/2110spring2016repo/commons/src/DequeQueue.h"
#include "X:/2110spring2016repo/commons/src/Queue.h"

template<class T>
class FIFOStrategy: public Strategy<T> {
private:
	Queue<T>* queue;
public:
	FIFOStrategy();
	~FIFOStrategy();

	bool isEmpty();
	bool add(T* element);
	T* remove();
	T* get();
};

template<class T>
FIFOStrategy<T>::FIFOStrategy() {
	queue = new DequeQueue<T>();
}

template<class T>
FIFOStrategy<T>::~FIFOStrategy() {
	delete queue;
}

template<class T>
bool FIFOStrategy<T>::isEmpty() {
	return queue->isEmpty();
}

template<class T>
bool FIFOStrategy<T>::add(T* element) {
	return queue->enqueue(element);
}

template<class T>
T* FIFOStrategy<T>::remove() {
	return queue->dequeue();
}

template<class T>
T* FIFOStrategy<T>::get() {
	return queue->peek();
}

#endif /* FIFOSTRATEGY_H_ */
