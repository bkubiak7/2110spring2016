/*
 * LIFOStrategy.h
 *
 *  Created on: Mar 1, 2015
 *      Author: B
 */

#ifndef LIFOSTRATEGY_H_
#define LIFOSTRATEGY_H_

#include "Strategy.h"
#include "Widget.h"
#include "X:/2110spring2016repo/commons/src/DequeStack.h"
#include "X:/2110spring2016repo/commons/src/Stack.h"

template<class T>
class LIFOStrategy: public Strategy<T> {
private:
	Stack<T>* stack;
public:
	LIFOStrategy();
	~LIFOStrategy();

	bool isEmpty();
	bool add(T* element);
	T* remove();
	T* get();
};

template<class T>
LIFOStrategy<T>::LIFOStrategy() {
	stack = new DequeStack<T>();
}

template<class T>
LIFOStrategy<T>::~LIFOStrategy() {
	delete stack;
}

template<class T>
bool LIFOStrategy<T>::isEmpty() {
	return stack->isEmpty();
}

template<class T>
bool LIFOStrategy<T>::add(T* element) {
	return stack->push(element);
}

template<class T>
T* LIFOStrategy<T>::remove() {
	return stack->pop();
}

template<class T>
T* LIFOStrategy<T>::get() {
	return stack->peek();
}

#endif /* LIFOSTRATEGY_H_ */
