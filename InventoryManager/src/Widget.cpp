/*
 * Widget.cpp
 *
 *  Created on: Feb 28, 2015
 *      Author: Beata
 */

#include "Widget.h"

unsigned int Widget::getAmount() {
	return amount;
}

void Widget::setAmount(unsigned int amount) {
	this->amount = amount;
}

float Widget::getCost() {
	return cost;
}

void Widget::setCost(float cost) {
	this->cost = cost;
}
