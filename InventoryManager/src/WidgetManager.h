/*
 * WidgetManager.h
 *
 *  Created on: Mar 1, 2015
 *      Author: B
 */

#ifndef WIDGETMANAGER_H_
#define WIDGETMANAGER_H_

#include "Strategy.h"
#include "Widget.h"

class WidgetManager {
private:
	Strategy<Widget>* strategy;
	float totalProfit;
public:
	WidgetManager(Strategy<Widget>* strategy);
	~WidgetManager();
	void buyWidgets(double cost, int numToBuy);
	double getTotalProfit();
	double sellWidgets(double cost, int numToSell);
};

#endif /* WIDGETMANAGER_H_ */
