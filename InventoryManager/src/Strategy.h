/*
 * Strategy.h
 *
 *  Created on: Mar 1, 2015
 *      Author: B
 */

#ifndef STRATEGY_H_
#define STRATEGY_H_

template<class T>
class Strategy {
public:
	virtual ~Strategy() {
	}
	virtual bool isEmpty() = 0;

	virtual bool add(T* element) = 0;

	virtual T* remove() = 0;

	virtual T* get() = 0;
};

#endif /* STRATEGY_H_ */
