/*
 * WidgetManager.cpp
 *
 *  Created on: Mar 1, 2015
 *      Author: B
 */

#include "WidgetManager.h"

WidgetManager::WidgetManager(Strategy<Widget>* strategy) {
	this->strategy = strategy;
	this->totalProfit = 0.0;
}

WidgetManager::~WidgetManager() {

}

void WidgetManager::buyWidgets(double cost, int numToBuy) {
	//TODO
}

double WidgetManager::getTotalProfit() {
	return totalProfit;
}

double WidgetManager::sellWidgets(double cost, int numToSell) {
	//TODO
	return 0.0;
}
