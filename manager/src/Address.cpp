/*
 * Address.cpp
 *
 *  Created on: Jan 27, 2016
 *      Author: bkubiak
 */

#include "Address.h"

void Address::setZipCode(int newZipCode) {
	zipCode = newZipCode;
}

int Address::getZipCode() {
	return zipCode;
}
