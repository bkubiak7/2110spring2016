/*
 * Person.h
 *
 *  Created on: Jan 27, 2016
 *      Author: bkubiak
 */

#ifndef PERSON_H_
#define PERSON_H_

#include <iostream>
using namespace std;

#include "Address.h"

class Person {
private:
	string lastName;
	int height;
	Address* address;
public:
	void setHeight(int newHeight);
	int getHeight();

	void setAddress(Address* newAddress);
	Address* getAddress();

	void printAllInfo();

	Person();
	Person(int height);
	~Person();
};

#endif /* PERSON_H_ */
