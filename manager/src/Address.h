/*
 * Address.h
 *
 *  Created on: Jan 27, 2016
 *      Author: bkubiak
 */

#ifndef ADDRESS_H_
#define ADDRESS_H_

#include <iostream>
using namespace std;

class Address {
private:
	string city;
	int zipCode;
public:
	void setZipCode(int newZipCode);
	int getZipCode();
};

#endif /* ADDRESS_H_ */
