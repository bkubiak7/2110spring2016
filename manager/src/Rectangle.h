/*
 * Rectangle.h
 *
 *  Created on: Feb 3, 2016
 *      Author: bkubiak
 */

#ifndef RECTANGLE_H_
#define RECTANGLE_H_

class Rectangle {
private:
	double height;
	double width;
public:
	double getHeight() const;
	void setHeight(double height);
	double getWidth() const;
	void setWidth(double width);


	double area();
	double perimeter();
};

#endif /* RECTANGLE_H_ */
