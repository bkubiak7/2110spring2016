/*
 * Person.cpp
 *
 *  Created on: Jan 27, 2016
 *      Author: bkubiak
 */

#include "Person.h"

void Person::setHeight(int newHeight) {
	height = newHeight;
}

int Person::getHeight() {
	return height;
}

void Person::setAddress(Address* newAddress) {
	address = newAddress;
}

Address* Person::getAddress() {
	return address;
}

void Person::printAllInfo() {
	cout << height << " " << address->getZipCode() << endl;
}

Person::Person() {
	height = 0;
	cout << "def constructor";
}

Person::Person(int height) {
	this->height = height;
	address = new Address();
	cout << "constructor that initializes height" << endl;
}

Person::~Person() {
	delete address;
	cout << "destructing" << endl;
}

