/*
 * Rectangle.cpp
 *
 *  Created on: Feb 3, 2016
 *      Author: bkubiak
 */

#include "Rectangle.h"

double Rectangle::getHeight() const {
	return height;
}

void Rectangle::setHeight(double height) {
	this->height = height;
}

double Rectangle::getWidth() const {
	return width;
}

void Rectangle::setWidth(double width) {
	this->width = width;
}

double Rectangle::area() {
	return height * width;
}

double Rectangle::perimeter() {
	return 2 * (width + height);
}
