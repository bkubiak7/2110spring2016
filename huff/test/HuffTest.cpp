#include <iostream>
using namespace std;
#include "gtest/gtest.h"
#include "../src/HuffmanTree.h"

TEST(HuffmanTree, 2FirstASCIIChars) {
	unsigned int* frequencies = new unsigned int[256]();
	frequencies[0] = 2;
	frequencies[1] = 3;
	HuffmanTree* tree = new HuffmanTree(frequencies);
	BTNode<InfoNode>* root = tree->getRoot();
	//EXPECT_EQ(5, root->getElement()->getFrequency());
	//EXPECT_EQ(2, root->getLeftChild()->getElement()->getFrequency());
	//EXPECT_EQ(3, root->getRightChild()->getElement()->getFrequency());
	delete frequencies;
	frequencies = NULL;
	delete tree;
	tree = NULL;
}

TEST(HuffmanTree, 4FirstASCIIChars) {
	unsigned int* frequencies = new unsigned int[256]();
	frequencies[0] = 2;
	frequencies[1] = 6;
	frequencies[2] = 3;
	frequencies[3] = 9;
	HuffmanTree* tree = new HuffmanTree(frequencies);
	BTNode<InfoNode>* root = tree->getRoot();
	/*EXPECT_EQ(20, root->getElement()->getFrequency());
	BTNode<InfoNode>* rootL = root->getLeftChild();
	BTNode<InfoNode>* rootR = root->getRightChild();
	EXPECT_EQ(9, rootL->getElement()->getFrequency());
	EXPECT_TRUE(rootL->isLeaf());
	EXPECT_EQ(11, rootR->getElement()->getFrequency());
	BTNode<InfoNode>* rootRL = rootR->getLeftChild();
	BTNode<InfoNode>* rootRR = rootR->getRightChild();
	EXPECT_EQ(5, rootRL->getElement()->getFrequency());
	EXPECT_EQ(6, rootRR->getElement()->getFrequency());
	EXPECT_TRUE(rootRR->isLeaf());
	BTNode<InfoNode>* rootRLL = rootRL->getLeftChild();
	BTNode<InfoNode>* rootRLR = rootRL->getRightChild();
	EXPECT_EQ(2, rootRLL->getElement()->getFrequency());
	EXPECT_EQ(3, rootRLR->getElement()->getFrequency());
	EXPECT_TRUE(rootRLL->isLeaf());
	EXPECT_TRUE(rootRLR->isLeaf());*/
	delete frequencies;
	frequencies = NULL;
	delete tree;
	tree = NULL;
}

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
