/*
 * InfoNode.cpp
 *
 *  Created on: Apr 4, 2015
 *      Author: Beata
 */

#include "InfoNode.h"

InfoNode::InfoNode(char newCharacter, unsigned int newFrequency) {
	character = newCharacter;
	frequency = newFrequency;
}

InfoNode::InfoNode(unsigned int newFrequency) {
	frequency = newFrequency;
}

char InfoNode::getCharacter() {
	return character;
}

void InfoNode::setCharacter(char character) {
	this->character = character;
}

unsigned int InfoNode::getFrequency() {
	return frequency;
}

void InfoNode::setFrequency(unsigned int frequency) {
	this->frequency = frequency;
}

int InfoNode::compare(const InfoNode& newInfoNode) {
	if (this->frequency < newInfoNode.frequency) {
		return -1;
	} else if (this->frequency > newInfoNode.frequency) {
		return 1;
	}
	return 0;
}

bool InfoNode::operator ==(const InfoNode& newInfoNode) {
	return !compare(newInfoNode);
}

bool InfoNode::operator <(const InfoNode& newInfoNode) {
	return compare(newInfoNode) < 0;
}

bool InfoNode::operator >(const InfoNode& newInfoNode) {
	return compare(newInfoNode) > 0;
}
