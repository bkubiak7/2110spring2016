/*
 * huff.cpp
 *
 *  Created on: Apr 4, 2015
 *      Author: Beata
 */
#include "HuffmanTree.h"
#include "BTNode.h"
#include "InfoNode.h"
int main(int argc, char **argv) {
	//--------test1-2FirstASCIIChars
	unsigned int* frequencies = new unsigned int[256]();
	frequencies[0] = 2;
	frequencies[1] = 3;
	HuffmanTree* tree = new HuffmanTree(frequencies);
	BTNode<InfoNode>* root = tree->getRoot();
	//cout << (5 == root->getElement()->getFrequency()) << endl;
	//cout << (2 == root->getLeftChild()->getElement()->getFrequency()) << endl;
	//cout << (3 == root->getRightChild()->getElement()->getFrequency()) << endl;
	delete frequencies;
	frequencies = NULL;
	delete tree;
	tree = NULL;
}
