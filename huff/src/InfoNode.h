/*
 * InfoNode.h
 *
 *  Created on: Apr 4, 2015
 *      Author: Beata
 */

#ifndef INFONODE_H_
#define INFONODE_H_

class InfoNode {
private:
	char character;
	unsigned int frequency;
public:
	InfoNode(char newCharacter, unsigned int newFrequency);
	InfoNode(unsigned int newFrequency);
	char getCharacter();
	void setCharacter(char character);
	unsigned int getFrequency();
	void setFrequency(unsigned int frequency);

	int compare(const InfoNode& newInfoNode);
	bool operator ==(const InfoNode& newInfoNode);
	bool operator <(const InfoNode& newInfoNode);
	bool operator >(const InfoNode& newInfoNode);
};

#endif /* INFONODE_H_ */
