/*
 * HuffmanTree.cpp
 *
 *  Created on: Apr 4, 2015
 *      Author: Beata
 */

#include "HuffmanTree.h"
#include "SortedArrayList.h"

HuffmanTree::~HuffmanTree() {
	destroy(root);
}

void HuffmanTree::destroy(BTNode<InfoNode>* node) {
	if (node != NULL) {
		destroy(node->getLeftSubtree());
		destroy(node->getRightSubtree());
		delete node->getElement();
		node->setElement(NULL);
		delete node;
		node = NULL;
	}
}

HuffmanTree::HuffmanTree(unsigned int* frequencies) {
	root = NULL;
//TODO
}

BTNode<InfoNode>* HuffmanTree::getRoot() {
	return root;
}

void HuffmanTree::setRoot(BTNode<InfoNode>* root) {
	this->root = root;
}
