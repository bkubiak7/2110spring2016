/*
 * HuffmanTree.h
 *
 *  Created on: Apr 4, 2015
 *      Author: Beata
 */

#ifndef HUFFMANTREE_H_
#define HUFFMANTREE_H_

#include "BTNode.h"
#include "InfoNode.h"

class HuffmanTree {
private:
	BTNode<InfoNode>* root;

	void destroy(BTNode<InfoNode>* node);
public:
	/**
	 * Builds Huffman Tree on the basis of the number of occurrences of characters.
	 */
	HuffmanTree(unsigned int* frequencies);
	~HuffmanTree();
	BTNode<InfoNode>* getRoot();
	void setRoot(BTNode<InfoNode>* root);
};

#endif /* HUFFMANTREE_H_ */
