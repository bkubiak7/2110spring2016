/*
 * ArrayQueue.h
 *
 *  Created on: Mar 16, 2016
 *      Author: bkubiak
 */

#ifndef ARRAYQUEUE_H_
#define ARRAYQUEUE_H_

#include <iostream>
using namespace std;

template<class T>
class ArrayQueue {
private:
	T** elements;
	int maxLength;
	int front;
	int back;
	int length;

	void resize();
public:
	ArrayQueue();
	virtual ~ArrayQueue();
	bool enqueue(T* element);
	T* dequeue();
	T* peek();
	bool isEmpty();
};
template<class T>
ArrayQueue<T>::ArrayQueue() {
	maxLength = 100;
	elements = new T*[maxLength];
	for (int var = 0; var < maxLength; ++var) {
		elements[var] = NULL;
	}
	front = 0;
	back = maxLength - 1;
	length = 0;
}

template<class T>
ArrayQueue<T>::~ArrayQueue() {
	delete[] elements;
}

template<class T>
bool ArrayQueue<T>::enqueue(T* element) {
	if (element == NULL) {
		return false;
	}
	bool notBigEnough = (maxLength == length);
	if (notBigEnough) {
		resize();
	}
	back = back + 1;
	if (back == maxLength) {
		back = 0;
	}

	elements[back] = element;
	length++;
	return true;
}

template<class T>
T* ArrayQueue<T>::dequeue() {
	if (isEmpty())
		return NULL;
	T* removed = elements[front];
	elements[front] = NULL;
	front++;
	if (front == maxLength)
		front = 0;
	length--;
	return removed;

}

template<class T>
T* ArrayQueue<T>::peek() {
	return elements[front];
}

template<class T>
bool ArrayQueue<T>::isEmpty() {
	return length == 0;
}

template<class T>
void ArrayQueue<T>::resize() {
	maxLength = 2 * maxLength;
	T** oldArray = elements;
	elements = new T*[maxLength];
	int counter = 0;
	for (int var = front; var < length; ++var) {
		elements[counter] = oldArray[var];
		counter++;
	}
	for (int var = 0; var < back; ++var) {
		elements[counter] = oldArray[var];
		counter++;
	}
	for (int var = length; var < maxLength; ++var) {
		elements[var] = NULL;
	}
	front = 0;
	back = length - 1;
	delete[] oldArray;
	oldArray = NULL;
}

#endif /* ARRAYQUEUE_H_ */
