/*
 * Set.h
 *
 *  Created on: May 3, 2015
 *      Author: bkubiak
 */

#ifndef SET_H_
#define SET_H_

template<class T>
class Set {
public:
	virtual ~Set();

	/**
	 * Gets the current number of elements in the set.
	 * @return The integer number of elements in the set.
	 */
	virtual int size() = 0;

	/**
	 * Inserts the specified element into the set.
	 * @param[in] element The element that has to be inserted into the set.
	 * @post If the element is not NULL then addition is successful
	 * i. e. the element is inserted into the set and
	 * the number of elements in the set is increased by 1.
	 * @return TRUE if the element was successfully added, FALSE otherwise.
	 */
	virtual bool add(T* element) = 0;

	/**
	 * Removes one occurrence of a given element from the set if it appears in the set.
	 * @param[in] element The element that has to be removed from the set.
	 * @post If the element is not null and it appears in the set, then
	 * it is removed from the set and the number of elements in the set is decreased by 1.
	 * Otherwise the set remains unchanged.
	 * @return FALSE if the element is NULL or if the element is not present in the set.
	 * TRUE if removal was successful.
	 */
	virtual bool remove(T* element) = 0;

	/**
	 * Removes all occurrences of a given element from the set if it appears in the set.
	 * @param[in] element The element that has to be removed from the set.
	 * @post If the element is not null and it appears in the set, then
	 * all its occurrences (integer number n, n>=1) are removed from the set and
	 * the number of elements in the set is decreased by n.
	 * Otherwise the set remains unchanged.
	 * @return -1 if the element is NULL, 0 if the element is not present in the set,
	 * integer number n (n>=1) if the element appeared n times in the set.
	 */
	virtual int removeAll(T* element) = 0;

	/**
	 * Counts the occurrences of a given element in the set.
	 * @param[in] element The element to be counted.
	 * @return -1 if the element is NULL, 0 if the element is not present in the set,
	 * integer number n (n>=1) if the element appears n times in the set.
	 */
	virtual int getFrequencyOf(T* element) = 0;

	/**
	 * Verifies if the given element appears in the set.
	 * @param[in] element The element to locate.
	 * @return TRUE if the set contains the element, FALSE otherwise.
	 */
	virtual bool contains(T* element) = 0;

};

template<class T>
Set<T>::~Set(void) {
}
#endif /* SET_H_ */
