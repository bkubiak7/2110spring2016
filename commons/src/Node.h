/*
 * Node.h
 *
 *  Created on: Feb 26, 2016
 *      Author: bkubiak
 */

#ifndef NODE_H_
#define NODE_H_

#include <iostream>
using namespace std;

template<class T>
class Node {
private:
	T* element;
	Node<T>* nextNode;

public:
	Node();
	Node(T* element);
	T* getElement();
	void setElement(T* element);
	Node<T>* getNext();
	void setNext(Node<T>* nextNode);
};

template<class T>
Node<T>::Node() {
	element = NULL;
	nextNode = NULL;
}

template<class T>
Node<T>::Node(T* element) {
	this->element = element;
	nextNode = NULL;
}

template<class T>
T* Node<T>::getElement() {
	return element;
}

template<class T>
void Node<T>::setElement(T* element) {
	this->element = element;
}

template<class T>
Node<T>* Node<T>::getNext() {
	return nextNode;
}

template<class T>
void Node<T>::setNext(Node<T>* nextNode) {
	this->nextNode = nextNode;
}

#endif /* NODE_H_ */
