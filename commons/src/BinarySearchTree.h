/*
 * BinarySearchTree.h
 *
 *  Created on: Apr 6, 2015
 *      Author: Beata
 */

#ifndef BINARYSEARCHTREE_H_
#define BINARYSEARCHTREE_H_

#include "LinkedList.h"

template<class T>
class BinarySearchTree {
public:
	virtual ~BinarySearchTree();

	/**
	 * Adds an element to the Binary Search Tree.
	 * @param[in] element The element that has to be inserted into the Binary Search Tree.
	 * @post The Binary Search Tree contains a new node.
	 * @return TRUE if the element was successfully added, FALSE if the element is NULL.
	 * In the latter case it will not be added to the tree.
	 */
	virtual bool add(T* element) = 0;

	/**
	 * Returns a height of the Binary Search Tree.
	 */
	virtual int getHeight() = 0;
};

template<class T>
BinarySearchTree<T>::~BinarySearchTree(void) {
}

#endif /* BINARYSEARCHTREE_H_ */
