/*
 * Queue.h
 *
 *  Created on: Feb 27, 2015
 *      Author: B
 */

#ifndef QUEUECREATOR__SRC_QUEUE_H_
#define QUEUECREATOR__SRC_QUEUE_H_

template<class T>
class Queue {
public:
	virtual ~Queue();

	virtual bool isEmpty() = 0;

	virtual bool enqueue(T* element) = 0;

	virtual T* dequeue() = 0;

	virtual T* peek() = 0;
};

template<class T>
Queue<T>::~Queue(void) {
}

#endif /* QUEUECREATOR__SRC_QUEUE_H_ */
