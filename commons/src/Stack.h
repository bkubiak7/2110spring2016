/*
 * Stack.h
 *
 *  Created on: Feb 20, 2015
 *      Author: B
 */

#ifndef STACKCREATOR__SRC_STACK_H_
#define STACKCREATOR__SRC_STACK_H_

template<class T>
class Stack {
public:
	virtual ~Stack();

	virtual bool isEmpty() = 0;

	virtual bool push(T* element) = 0;

	virtual T* pop() = 0;

	virtual T* peek() = 0;
};

template<class T>
Stack<T>::~Stack(void) {
}

#endif /* STACKCREATOR__SRC_STACK_H_ */
