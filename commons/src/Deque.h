/*
 * Deque.h
 *
 *  Created on: Feb 28, 2015
 *      Author: Beata
 */

#ifndef DEQUE_H_
#define DEQUE_H_

template<class T>
class Deque {
public:
	virtual ~Deque();

	/**
	 * Gets the current number of elements in the list.
	 * @return The integer number of elements in the list.
	 */
	virtual int size() = 0;

	virtual bool enqueueFront(T* element) = 0;

	virtual T* dequeueFront() = 0;

	virtual T* peekFront() = 0;

	virtual bool enqueueBack(T* element) = 0;

	virtual T* dequeueBack() = 0;

	virtual T* peekBack() = 0;
};

template<class T>
Deque<T>::~Deque(void) {
}
#endif /* DEQUE_H_ */
