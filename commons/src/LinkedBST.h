/*
 * LinkedBST.h
 *
 *  Created on: Apr 6, 2015
 *      Author: Beata
 */

#ifndef LINKEDBST_H_
#define LINKEDBST_H_

#include "BinarySearchTree.h"
#include "BTNode.h"
#include "LinkedList.h"
#include <cmath>

template<class T>
class LinkedBST: public BinarySearchTree<T> {
private:
	BTNode<T>* root;

	void destroy(BTNode<T>* node);

	BTNode<T>* add(BTNode<T>* currentNode, BTNode<T>* newNode);

	int calculateHeight(BTNode<T>* currentNode);

public:
	LinkedBST();
	~LinkedBST();
	bool add(T* element);
	int getHeight();
};

template<class T>
LinkedBST<T>::LinkedBST() {
	root = NULL;
}

template<class T>
LinkedBST<T>::~LinkedBST() {
	destroy(root);
}

template<class T>
void LinkedBST<T>::destroy(BTNode<T>* node) {
	if (node != NULL) {
		destroy(node->getLeftSubtree());
		destroy(node->getRightSubtree());
		delete node;
		node = NULL;
	}
}

template<class T>
int LinkedBST<T>::getHeight() {
	return calculateHeight(root);
}

template<class T>
int LinkedBST<T>::calculateHeight(BTNode<T>* currentNode) {
	if (currentNode == NULL) {
		return 0;
	}
	int leftSubtreeHeight = calculateHeight(currentNode->getLeftSubtree());
	int rightSubtreeHeight = calculateHeight(currentNode->getRightSubtree());
	return 1 + max(leftSubtreeHeight, rightSubtreeHeight);
}

template<class T>
bool LinkedBST<T>::add(T* element) {
	if (element == NULL) {
		return false;
	}

	BTNode<T>* newNode = new BTNode<T>(element);
	root = add(root, newNode);
	return true;
}

template<class T>
BTNode<T>* LinkedBST<T>::add(BTNode<T>* currentNode, BTNode<T>* newNode) {
	if (currentNode == NULL) {
		return newNode;
	} else if (*currentNode->getElement() > *newNode->getElement()) {
		BTNode<T>* temp = add(currentNode->getLeftSubtree(), newNode);
		currentNode->setLeftSubtree(temp);
	} else if (*currentNode->getElement() < *newNode->getElement()) {
		BTNode<T>* temp = add(currentNode->getRightSubtree(), newNode);
		currentNode->setRightSubtree(temp);
	}
	return currentNode;
}

#endif /* LINKEDBST_H_ */
