//============================================================================
// Name        : Driver.cpp
// Author      : 
// Version     :
// Copyright   :
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

#include <stack>

#include "Stack.h"
#include "ArrayStack.h"
#include "LinkedList.h"

int main() {
	LinkedList<int> myList;
	int x1 = 5;
	int x2 = 6;
	myList.add(0, &x1);
	myList.add(1, &x2);
	for (int var = 0; var < myList.size(); ++var) {
		cout << *myList.get(var);
	}

	LinkedListIterator<int>* iter = myList.getIterator();
	while (iter->hasNext()) {

		cout << *iter->next();
	}

	return 0;
}
