/*
 * Graph.cpp
 *
 *  Created on: Mar 22, 2015
 *      Author: bkubiak
 */

#include "Graph.h"
#include "ArrayList.h"

Graph::Graph() {
	vertices = new ArrayList<Vertex>();
}

Graph::~Graph() {
	delete vertices;
}

bool Graph::addVertex(Vertex* vertex) {
	if (vertex == NULL) {
		return false;
	}
	vertices->add(vertices->size(), vertex);
	return true;
}

bool Graph::addEdge(Vertex* vertex1, Vertex* vertex2) {
	if (vertex1 == NULL || vertex2 == NULL) {
		return false;
	}
	vertex1->addNeighbor(vertex2);
	vertex2->addNeighbor(vertex1);
	return true;
}

Vertex* Graph::getVertex(int value) {
	Vertex* currVertex = NULL;
	for (int var = 0; var < vertices->size(); ++var) {
		currVertex = vertices->get(var);
		if (currVertex->getValue() == value) {
			return currVertex;
		}
	}
	return currVertex;
}
