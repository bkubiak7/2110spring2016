/*
 * GraphWalkerDriver.cpp
 *
 *  Created on: Mar 22, 2015
 *      Author: bkubiak
 */

#include <iostream>
using namespace std;

#include "ArrayList.h"
#include "Graph.h"
#include "Vertex.h"
#include "Walker.h"

int main(int argc, char **argv) {
	Vertex v1(1);
	Vertex v2(2);
	Vertex v3(3);
	Vertex v4(4);
	Vertex v5(5);
	Vertex v6(6);
	Vertex v7(7);
	Vertex v8(8);
	Graph graph;
	graph.addVertex(&v1);
	graph.addVertex(&v2);
	graph.addVertex(&v3);
	graph.addVertex(&v4);
	graph.addVertex(&v5);
	graph.addVertex(&v6);
	graph.addVertex(&v7);
	graph.addVertex(&v8);
	graph.addEdge(&v1, &v2);
	graph.addEdge(&v1, &v4);
	graph.addEdge(&v1, &v7);
	graph.addEdge(&v2, &v5);
	graph.addEdge(&v2, &v6);
	graph.addEdge(&v3, &v6);
	graph.addEdge(&v3, &v8);
	graph.addEdge(&v4, &v6);
	graph.addEdge(&v5, &v7);

	Walker walker;
	ArrayList<Vertex>* result = walker.breadthFirstSearch(&v1);
	for (int var = 0; var < result->size(); ++var) {
		cout << result->get(var)->getValue() << endl;
	}
}
