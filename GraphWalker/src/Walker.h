/*
 * Walker.h
 *
 *  Created on: Mar 22, 2015
 *      Author: bkubiak
 */

#ifndef WALKER_H_
#define WALKER_H_

#include "ArrayList.h"
#include "Vertex.h"

class Walker {
public:
	ArrayList<Vertex>* breadthFirstSearch(Vertex* vertex);
	ArrayList<Vertex>* depthFirstSearch(Vertex* vertex);
};

#endif /* WALKER_H_ */
