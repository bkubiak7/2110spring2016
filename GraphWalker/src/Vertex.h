/*
 * Vertex.h
 *
 *  Created on: Mar 22, 2015
 *      Author: bkubiak
 */

#ifndef VERTEX_H_
#define VERTEX_H_

#include <iostream>
using namespace std;

#include "SortedArrayList.h"
#include "Vertex.h"

class Vertex {
private:
	int value;
	bool visited;
	SortedArrayList<Vertex>* neighbors;
public:
	Vertex(int newValue);
	~Vertex();
	bool addNeighbor(Vertex* newNeighbor);
	int getValue();
	bool getVisited();
	void setVisited(bool visited);
	SortedArrayList<Vertex>* getNeighbors();

	int compare(const Vertex& otherVertex) const;
	bool operator ==(const Vertex& otherVertex) const;
	bool operator <(const Vertex& otherVertex) const;
};

#endif /* VERTEX_H_ */
