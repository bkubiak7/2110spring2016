/*
 * Graph.h
 *
 *  Created on: Mar 22, 2015
 *      Author: bkubiak
 */

#ifndef GRAPH_H_
#define GRAPH_H_

#include "ArrayList.h"
#include "Vertex.h"

class Graph {
private:
	ArrayList<Vertex>* vertices;
public:
	Graph();
	~Graph();

	bool addVertex(Vertex* vertex);
	bool addEdge(Vertex* vertex1, Vertex* vertex2);
	Vertex* getVertex(int value);
};

#endif /* GRAPH_H_ */
