/*
 * StackerTester.cpp
 *
 *  Created on: Feb 8, 2016
 *      Author: bkubiak
 */

#include <iostream>
using namespace std;
#include "gtest/gtest.h"
#include "../src/StackUser.h"

TEST(StackUser_areBracesBalanced, twoOpeningOneClosing) {
	string text = "s{{s}s";
	EXPECT_FALSE(StackUser::areBracesBalanced(text));
}

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
