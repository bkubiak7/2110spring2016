/*
 * StackUser.cpp
 *
 *  Created on: Feb 8, 2016
 *      Author: bkubiak
 */

#include <iostream>
using namespace std;

#include "ArrayStack.h"

class StackUser {
public:
	/**
	 * Checks if a given text has balanced curly brackets.
	 * @param[in] text Text that has to be checked for balanced brackets.
	 * @return TRUE if the brackets in the text are balanced, FALSE otherwise.
	 */
	static bool areBracesBalanced(string text) {
		ArrayStack<char> stack;
		stack.push();
		return false;
	}

	/**
	 * Checks if a given word is a palindrome.
	 * @param[in] word Word that has to verified.
	 * @return TRUE if the word is a palindrome, FALSE otherwise.
	 */
	static bool isSymmetric(string text) {
		return false;
	}
};
