//============================================================================
// Name        : lab11.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

#include "Person.h"

int main() {
	string lastName1 = "Brown";
	string password1 = "Brown123";
	string lastName2 = "Blue";
	string password2 = "Blue123";
	string lastName3 = "Blue";
	string password3 = "Black123";
	Person p1(lastName1, password1), p2(lastName2, password2), p3(lastName3,
			password3);
	if (p2 < p3) {
		cout << p2.getLastName() + " " + p2.getPassword();
	} else
		cout << p3.getLastName() + " " + p3.getPassword();

	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!
	return 0;
}
