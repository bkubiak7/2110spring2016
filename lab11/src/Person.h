/*
 * Person.h
 *
 *  Created on: Jan 23, 2015
 *      Author: bkubiak
 */

#ifndef ATM_SRC_PERSON_H_
#define ATM_SRC_PERSON_H_

#include <iostream>
using namespace std;

class Person {
private:
	string lastName;
	string password;
public:
	Person(string& newLastName, string& newPassword);
	~Person();

	string& getLastName();
	void setLastName(string& newLastName);

	string& getPassword();
	void setPassword(string& password);

	int compare(const Person& person) const;
	bool operator ==(const Person& d) const;
	bool operator <(const Person& d) const;
};

#endif /* ATM_SRC_PERSON_H_ */
