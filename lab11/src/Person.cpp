/*
 * Person.cpp
 *
 *  Created on: Jan 23, 2015
 *      Author: bkubiak
 */

#include "Person.h"

Person::Person(string& newLastName, string& newPassword) {
	lastName = newLastName;
	password = newPassword;
}

Person::~Person() {
}

string& Person::getLastName() {
	return lastName;
}

void Person::setLastName(string& newLastName) {
	lastName = newLastName;
}

string& Person::getPassword() {
	return password;
}

void Person::setPassword(string& password) {
	this->password = password;
}

int Person::compare(const Person& person) const {
	if (lastName < person.lastName) {
		return -1;
	} else if (lastName > person.lastName) {
		return 1;
	} else if (password < person.password) {
		return -1;
	} else if (password > person.password) {
		return 1;
	}
	return 0;
}

bool Person::operator ==(const Person& d) const {
	return !compare(d);
}

bool Person::operator <(const Person& d) const {
	return compare(d) < 0;
}
