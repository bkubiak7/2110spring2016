/*
 * QeueueTests.cpp
 *
 *  Created on: Mar 16, 2016
 *      Author: bkubiak
 */

#include <iostream>
using namespace std;
#include "gtest/gtest.h"
#include "../src/SortedArrayList.h"

TEST(SortedArrayList, addToEmptySortedList) {
	SortedArrayList<int> myList;
	int myInt = 13;
	int position = myList.add(&myInt);
	EXPECT_EQ(0, position);

	EXPECT_EQ(1, myList.size());
}

TEST(SortedArrayList, addNULLEmptySortedList) {
	SortedArrayList<int> myList;
	int position = myList.add(NULL);
	EXPECT_EQ(-1, position);

	EXPECT_EQ(0, myList.size());
}

TEST(SortedArrayList, addThreeElems) {
	SortedArrayList<int> myList;
	int myInt1 = 13;
	int myInt2 = 33;
	int myInt3 = 23;
	int position = myList.add(&myInt1);
	EXPECT_EQ(0, position);
	position = myList.add(&myInt2);
	EXPECT_EQ(1, position);
	position = myList.add(&myInt3);
	EXPECT_EQ(1, position);

	EXPECT_EQ(3, myList.size());
}

TEST(SortedArrayList, removeNULLEmptyQueueSortedList) {
	SortedArrayList<int> myList;
	int* element = NULL;
	bool removed = myList.remove(element);

	EXPECT_FALSE(removed);
}

TEST(SortedArrayList, addOneRemoveOneSortedList) {
	SortedArrayList<int> myList;
	int myInt = 13;
	myList.add(&myInt);
	int myIntForRemoval = 13;
	bool removed = myList.remove(&myIntForRemoval);

	EXPECT_TRUE(removed);
}

