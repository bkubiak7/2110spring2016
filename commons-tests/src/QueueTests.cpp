/*
 * QeueueTests.cpp
 *
 *  Created on: Mar 16, 2016
 *      Author: bkubiak
 */

#include <iostream>
using namespace std;
#include "gtest/gtest.h"
#include "../src/ArrayQueue.h"

TEST(ArrayQueue, addToEmptyQueue) {
	ArrayQueue<int> myQueue;
	int myInt = 13;
	bool enqueued = myQueue.enqueue(&myInt);
	EXPECT_TRUE(enqueued);

	EXPECT_FALSE(myQueue.isEmpty());
}

TEST(ArrayQueue, addNULLEmptyQueue) {
	ArrayQueue<int> myQueue;
	bool enqueued = myQueue.enqueue(NULL);
	EXPECT_FALSE(enqueued);

	EXPECT_TRUE(myQueue.isEmpty());
}

TEST(ArrayQueue, addThreeElems) {
	ArrayQueue<int> myQueue;
	int myInt1 = 13;
	int myInt2 = 33;
	int myInt3 = 23;
	bool enqueued = myQueue.enqueue(&myInt1);
	EXPECT_TRUE(enqueued);
	enqueued = myQueue.enqueue(&myInt2);
	EXPECT_TRUE(enqueued);
	enqueued = myQueue.enqueue(&myInt3);
	EXPECT_TRUE(enqueued);

	EXPECT_FALSE(myQueue.isEmpty());
}

TEST(ArrayQueue, dequeueEmptyQueue) {
	ArrayQueue<int> myQueue;
	int* element = myQueue.dequeue();

	EXPECT_TRUE(element == NULL);
}

TEST(ArrayQueue, enqueueThreeElemsDequeueOnce) {
	ArrayQueue<int> myQueue;
	int myInt1 = 13;
	int myInt2 = 33;
	int myInt3 = 23;
	myQueue.enqueue(&myInt1);
	myQueue.enqueue(&myInt2);
	myQueue.enqueue(&myInt3);

	int* element = myQueue.dequeue();
	EXPECT_EQ(13, *element);

	EXPECT_FALSE(myQueue.isEmpty());
}

TEST(ArrayQueue, enqueueThreeElemsDequeueAll) {
	ArrayQueue<int> myQueue;
	int myInt1 = 13;
	int myInt2 = 33;
	int myInt3 = 23;
	myQueue.enqueue(&myInt1);
	myQueue.enqueue(&myInt2);
	myQueue.enqueue(&myInt3);

	int* element = myQueue.dequeue();
	EXPECT_EQ(13, *element);
	element = myQueue.dequeue();
	EXPECT_EQ(33, *element);
	element = myQueue.dequeue();
	EXPECT_EQ(23, *element);

	EXPECT_TRUE(myQueue.isEmpty());
}

TEST(ArrayQueue, enqueueThreeElemsPeek) {
	ArrayQueue<int> myQueue;
	int myInt1 = 13;
	int myInt2 = 33;
	int myInt3 = 23;
	myQueue.enqueue(&myInt1);
	myQueue.enqueue(&myInt2);
	myQueue.enqueue(&myInt3);

	int* element = myQueue.peek();
	EXPECT_EQ(13, *element);

	EXPECT_FALSE(myQueue.isEmpty());
}
